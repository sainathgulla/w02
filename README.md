** W02 **

** We'll set up some more tools, clone a code example, and explore a bit of Bootstrap and JavaScript. You can work with a partner or by your own, but submit separately (for example, your screenshot on your own laptop).**

** Keep track of how long it takes to complete this assignment. **

* Install Git for Windows from (https://git-scm.com/download/win). (Git is a version control system widely used to manage code as software projects evolve). 
* Install TortoiseSVN from (https://tortoisesvn.net/)
* Go to the code repository at: (https://github.com/denisecase/groupmaker)
* Get the code from the repository onto your local machine. You may read about how to clone it (using TortoiseSVN or the Git Bash window or any other tools) - or you can download the zipfile. 
* Change the name that appears in the menu from GroupMaker to  Yourname's GroupMaker. (You'll have to figure out where this occurs). 
* Experiment with the application - especially the BootStrap classes, styles, form inputs (or their attributes). Make at least one obvious change. 
* Without having learned JavaScript, try to figure out what the code in the .js file is doing. 

** To submit: **

* Title your post "NN Lastname, Firstname - success" if successful.  Title your post "NN Lastname, Firstname" if you run into any problems or have any questions. NN is your two-digit section, e.g. 03. Incorrect format on the post title = -10%. . 
* Embed a screenshot showing your modified GroupMaker web app with the new name running in Chrome on your desktop. 
* Embed a screenshot to show that you have installed VS Code. 
* Tell us how long the assignment took. 
* Tell us if you had any problems or if you have any questions. 
* Clarification: Do not attach files - display all images and code in your post (see the icon that looks like ).  Save your screenshot, upload it to tinypic.com. Enter the passphrase and select the last option "Direct link for layouts."